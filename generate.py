#!/usr/bin/env python3

import json
import os
import pycparser
import sys
import urllib.request

commit_url = "https://cgit.freedesktop.org/drm/drm/patch/?id=drm-next"
commit_str = urllib.request.urlopen(commit_url).read().decode()
commit_header = commit_str.split("\n\n")[0]
hash = commit_header.split("\n")[0].split(" ")[1]
print(commit_header, file=sys.stderr)

hash = "drm-next"
header_url = "https://cgit.freedesktop.org/drm/drm/plain/include/uapi/drm/drm_fourcc.h?h=" + hash
table_url = "https://cgit.freedesktop.org/drm/drm/plain/drivers/gpu/drm/drm_fourcc.c?h=" + hash

table_str = urllib.request.urlopen(table_url).read().decode()

prefix = "static const struct drm_format_info formats[] = {"
suffix = "};"

i = table_str.index(prefix)
assert(i >= 0)
table_str = table_str[i:]

i = table_str.index(suffix)
assert(i >= 0)
table_str = table_str[:i + len(suffix)]

def serialize_expr(expr):
    if isinstance(expr, pycparser.c_ast.ID):
        if expr.name == "true":
            return True
        elif expr.name == "false":
            return False
        else:
            return expr.name
    elif isinstance(expr, pycparser.c_ast.Constant):
        if expr.type == "int":
            return int(expr.value)
    elif isinstance(field_ast.expr, pycparser.c_ast.InitList):
        return [serialize_expr(v) for v in expr.exprs]
    raise Exception(f"Unhandled expression type: {expr}")

parser = pycparser.CParser()
table_ast = parser.parse(table_str, "drm_fourcc.c")
entries_ast = table_ast.ext[0].init.exprs
formats = []
for entry_ast in entries_ast:
    entry = {}
    for field_ast in entry_ast:
        k = field_ast.name[0].name
        entry[k] = serialize_expr(field_ast.expr)

    num_planes = entry["num_planes"]
    entry["format"] = entry["format"].removeprefix("DRM_FORMAT_")
    if not entry["depth"]:
        del(entry["depth"])
    for k in ["cpp", "char_per_block", "block_w", "block_h"]:
        if k in entry:
            for v in entry[k][num_planes:]:
                assert(not v)
            entry[k] = entry[k][:num_planes]
    for k in ["block_w", "block_h"]:
        if k not in entry:
            entry[k] = [1 for i in range(num_planes)]

    formats.append(entry)

with open(os.getenv("MESON_SOURCE_ROOT") + "/formats.json", "w") as f:
    json.dump(formats, f, indent=4)
